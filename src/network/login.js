import {request} from './request'

export function getPhoneLogin(phone, password) {
  return request({
    url: '/login/cellphone',
    params: {
      phone,
      password,
    }
  })
}

export function getPhoneLoginOut() {
  return request({
    url: 'logout',
  })
}

// export function getUserSonglist(uid) {
//   return request({
//     url: 'user/playlist',
//     params: {
//       uid,
//     }
//   })
// }
//xuqy：获取歌单
export function getUserSonglist(uid){
  var uid = JSON.parse(window.localStorage.getItem('currentUserInfo'))
  // console.log(window.localStorage.getItem('currentUserInfo').userId)
  // console.log(uid.userId)
  return request({
    url: 'http://127.0.0.1:80/songsheet/findByuserId/'+uid.userId,
  })
}

export function getUserAnchor(uid) {
  return request({
    url: '/user/dj',
    params: {
      uid,
    }
  })
}

export function getUserFollows(uid) {
  return request({
    url: '/user/follows',
    params: {
      uid,
    }
  })
}
