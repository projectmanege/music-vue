import {request} from './request'

//获取用户信息
export function getUserInfo(uid){
  return request({
    url: 'http://127.0.0.1:80/user/findByuserId/'+uid,
  })
}
//检查是否已关注
export function checkFaned(fanId,currentUserId){
  return request({
    url: 'http://127.0.0.1:80/IsFaned/'+fanId+'/'+currentUserId,
  })
}

//取消关注
export function quXiaoGuanZhu(fanId,currentUserId){
  return request({
    url: 'http://127.0.0.1:80/deleteFanedByuserId/'+fanId+'/'+currentUserId,
  })
}

//获取关注数
export function guanZhuShu(UserId){
  return request({
    url: 'http://127.0.0.1:80/countMyFaned/'+UserId,
  })
}
//获取粉丝数
export function getFansNum(UserId){
  return request({
    url: 'http://127.0.0.1:80/countMyFans/'+UserId,
  })
}

