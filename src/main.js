import Vue from 'vue'
// import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'
import './assets/css/reset.css'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import toast from 'components/common/toast/index'
import axios from 'axios'
Vue.use(toast);
Vue.config.productionTip = false
Vue.prototype.$bus = new Vue();

Vue.prototype.$axios = axios



// let config = {
// };
// // axios.defaults.baseURL = '/api'  //关键代码
//
// const _axios = axios.create(config);

// axios.interceptors.request.use(
//   function(config) {
//     // Do something before request is sent
//     config.headers.Authorization = window.localStorage.getItem("token")
//     return config;
//   },
//   function(error) {
//     // Do something with request error
//     return Promise.reject(error);
//   }
// );
// 添加请求拦截器
axios.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么
  // 判断是否存在token,如果存在将每个页面header添加token
  if (window.localStorage.getItem("token")) {
    config.headers.common['Authorization'] = window.localStorage.getItem("token");
  }
  return config
}, function (error) {

  return Promise.reject(error)
})

// Add a response interceptor
axios.interceptors.response.use(
  function(response) {
    // Do something with response data
    return response;
  },
  function(error) {
    // Do something with response error
    return Promise.reject(error);
  }
);

Vue.use(ElementUI);
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

